# -*- coding: utf-8 -*-


def test_gitlab_conf(gitlab):
    assert gitlab.api_url == 'https://gitlab.example.com/api/v4'
