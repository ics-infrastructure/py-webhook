# -*- coding: utf-8 -*-

import logging
import pytest
from click.testing import CliRunner
from pywebhook.cmd import cli
from requests.exceptions import HTTPError


API_URL = 'https://api.bitbucket.org/2.0/repositories/bitbucket_owner'
BITBUCKET_TOKEN_URL = 'https://bitbucket.org/site/oauth2/access_token'


@pytest.fixture(autouse=True)
def no_fetch_token(monkeypatch):
    def fetch_token(*args, **kwargs):
        return None
    monkeypatch.setattr("requests_oauthlib.OAuth2Session.fetch_token", fetch_token)


def test_webhook_from_conf_valid(bitbucket):
    webhook = bitbucket.webhook_from_conf("Webhook description")
    assert webhook == {'name': 'Webhook description',
                       'url': 'https://webhook-proxy.com/build',
                       'args': {
                           'jenkins': 'https://jenkins.mycompany.com',
                           'job': 'jenkins_job_name',
                           'token': 'my-secret-token',
                       }
                       }


def test_webhook_from_conf_invalid(bitbucket):
    assert bitbucket.webhook_from_conf("foo") is None


def test_one_page(bitbucket, request_mock):
    url = 'https://test.com'
    data = {'a': 'b', 'c': 2}
    request_mock.get(url, json=data)
    pages = list(bitbucket.pages(url))
    assert len(pages) == 1
    assert pages == [data]


def test_three_pages(bitbucket, request_mock):
    url1 = 'https://test.com'
    url2 = 'https://test.com/page2'
    url3 = 'https://test.com/page3'
    page1 = {'page': 1, 'next': url2, 'values': [1, 2, 3]}
    page2 = {'page': 2, 'next': url3, 'values': [4, 5, 6]}
    page3 = {'page': 3, 'values': [7, 8, 9]}
    request_mock.get(url1, json=page1)
    request_mock.get(url2, json=page2)
    request_mock.get(url3, json=page3)
    pages = list(bitbucket.pages(url1))
    assert len(pages) == 3
    assert pages == [page1, page2, page3]


def test_webhook_unknown(config_file, caplog):
    caplog.set_level(logging.INFO)
    name = 'Webhook not defined'
    runner = CliRunner()
    result = runner.invoke(cli, ['--conf', config_file, 'bitbucket', '-r', 'my_repo', name])
    assert result.exit_code == 1
    assert 'Unknown webhook "{}". Check your configuration. Abort.'.format(name) in caplog.text


def test_webhook_created(config_file, request_mock, caplog):
    caplog.set_level(logging.INFO)
    repo_name = 'my_repository'
    name = 'Webhook description'
    url = '{}/{}/hooks'.format(API_URL, repo_name)
    get_data = {'page': 1, 'values': [{'description': 'foo'}]}
    request_mock.get(url, status_code=200, json=get_data)
    request_mock.post(url, status_code=201)
    runner = CliRunner()
    result = runner.invoke(cli, ['--conf', config_file, 'bitbucket', '-r', repo_name, name])
    assert request_mock.call_count == 2
    assert result.exit_code == 0
    assert 'Webhook "{}" successfully created for {}'.format(name, repo_name) in caplog.text


def test_webhook_already_exists(config_file, request_mock, caplog):
    caplog.set_level(logging.INFO)
    repo_name = 'my_repository'
    name = 'Webhook description'
    url = '{}/{}/hooks'.format(API_URL, repo_name)
    get_data = {'page': 1, 'values': [{'description': name}]}
    request_mock.get(url, status_code=200, json=get_data)
    runner = CliRunner()
    result = runner.invoke(cli, ['--conf', config_file, 'bitbucket', '-r', repo_name, name])
    assert request_mock.call_count == 1
    assert result.exit_code == 0
    assert 'Skipping {}. Webhook "{}" already exists'.format(repo_name, name) in caplog.text


def test_webhook_not_created(config_file, request_mock, caplog):
    caplog.set_level(logging.INFO)
    repo_name = 'my_repository'
    name = 'Webhook description'
    url = '{}/{}/hooks'.format(API_URL, repo_name)
    get_data = {'page': 1, 'values': [{'description': 'foo'}]}
    request_mock.get(url, status_code=200, json=get_data)
    request_mock.post(url, status_code=503)
    runner = CliRunner()
    result = runner.invoke(cli, ['--conf', config_file, 'bitbucket', '-r', repo_name, name])
    assert request_mock.call_count == 2
    assert result.exit_code == 0
    assert 'Webhook "{}" NOT created for {}'.format(name, repo_name) in caplog.text


def test_webhook_exists_error(config_file, request_mock, caplog):
    caplog.set_level(logging.INFO)
    repo_name = 'my_repository'
    url = '{}/{}/hooks'.format(API_URL, repo_name)
    request_mock.get(url, status_code=500)
    runner = CliRunner()
    result = runner.invoke(cli, ['--conf', config_file, 'bitbucket', '-r', repo_name, 'Webhook description'])
    assert request_mock.call_count == 1
    assert isinstance(result.exception, HTTPError)
