# -*- coding: utf-8 -*-

import pytest
import requests_mock
from pywebhook.bitbucket import Bitbucket
from pywebhook.gitlab import Gitlab


@pytest.fixture
def config_file(tmpdir):
    f = tmpdir.join('bitbucket.yml')
    f.write("""bitbucket:
  owner: bitbucket_owner
  client_id: my_client_id
  client_secret: my_client_secret

gitlab:
  url: https://gitlab.example.com
  token: secret_token

webhooks:
  - name: Webhook description
    url: https://webhook-proxy.com/build
    args:
      jenkins: https://jenkins.mycompany.com
      job: jenkins_job_name
      token: my-secret-token
""")
    return f.strpath


@pytest.fixture
def bitbucket(config_file):
    return Bitbucket(config_file)


@pytest.fixture
def gitlab(config_file):
    return Gitlab(config_file)


@pytest.fixture
def request_mock():
    mocker = requests_mock.Mocker()
    mocker.start()
    yield mocker
    mocker.stop()
