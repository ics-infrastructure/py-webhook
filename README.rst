git-webhook
===========

Command line application to interact with Bitbucket and Gitlab webhooks.


Usage
-----

::

    $ py-webhook --help
    Usage: py-webhook [OPTIONS] COMMAND [ARGS]...
    
      Script to modify Bitbucket and Gitlab webhooks
    
    Options:
      -c, --conf TEXT       Configuration file [default: "~/.py-webhook.yml"].
      --debug / --no-debug  Set log level to debug.
      --version             Show the version and exit.
      --help                Show this message and exit.
    
    Commands:
      bitbucket  Modify a webhook on the specified Bitbucket...
      gitlab     Modify a webhook on the specified Gitlab...


The YAML configuration file should include the following::

        bitbucket:
          owner: bitbucket_owner
          client_id: bitbucket_client_id
          client_secret: bitbucket_client_secret

        gitlab:
          url: https://gitlab.example.com
          token: secret_token

        webhooks:
          - name: webhook name
            url: https://myproxy.mycompany.com/build
            push_events: False
            tag_push_events: True
            exclude: []
            args:
              jenkins: https://jenkins.mycompany.com
              job: jenkins_job_name
              token: jenkins_token

`push_events` and `tag_push_events` keywords are optional. They are only used for gitlab and default to True if not set.

`exclude` is also optional. If used, it shoud be set to a list of repo slug/project path to exclude (no webhook will be installed).



Installation
------------

`py-webhook` is a Python package that could be installed in a virtual environment.
To make it easier to deploy, it is recommended to create a pex_ file, which is a self-contained executable
Python environment.

You should first install the `pex` utility. See https://pex.readthedocs.io/en/stable/buildingpex.html.
Install `pex` in a virtualenv and create an executable::

    $ pip install pex
    $ pex pex requests -c pex -o ~/bin/pex

To create the `py-webhook` PEX file, run at the root of your clone::

    $ make pex

The created `py-webhook` file should run on any machine with the same OS and Python 2.7 installed.
It's a zip archive that contains all the required dependencies.
Note that PyYAML is not architecture independent. A pex file created on OSX will only run on OSX.
To run on Linux, the pex file shall be created on Linux.


Development
-----------

conda_ is the recommended way to create a separate environment.
It's possible to use virtualenv_ if you prefer.

1. Clone the repository

2. Create and activate a new conda environment::

   $ conda env create -n py-webhook -f environment.yml
   $ source activate py-webhook

3. Create an *editable* install::

   (py-webhook) $ pip install -e .

4. You can now run the `py-webhook` command::

   (py-webhook) $ py-webhook --help

5. To run the tests::

   (py-webhook) $ make test


If you want to run the tests using directly `pytest` you have to install the following extra packages in your environment:

- pytest
- requests-mock


.. _conda: https://conda.io/docs/using/envs.html
.. _virtualenv: https://virtualenv.pypa.io/en/stable/
.. _pex: https://pex.readthedocs.io/en/stable/index.html
