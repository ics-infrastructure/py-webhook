import os
import click
import logging
from .bitbucket import Bitbucket
from .gitlab import Gitlab
try:
    # Python 3
    from urllib.parse import quote_plus
except ImportError:
    # Python 2
    from urllib import quote_plus


@click.group()
@click.option('--conf', '-c', default='~/.py-webhook.yml',
              help='Configuration file [default: "~/.py-webhook.yml"].')
@click.option('--debug/--no-debug', default=False,
              help='Set log level to debug.')
@click.version_option()
@click.pass_context
def cli(ctx, conf, debug):
    """Script to modify Bitbucket and Gitlab webhooks"""
    logger = logging.getLogger('py-webhook')
    handler = logging.StreamHandler()
    formatter = logging.Formatter('%(asctime)s %(name)-12s %(levelname)-8s %(message)s')
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    if debug:
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)
    filepath = os.path.expanduser(conf)
    if not os.path.exists(filepath):
        raise click.BadParameter("Configuration file '{}' doesn't exist".format(filepath))
    ctx.obj = filepath


@cli.command()
@click.argument('name')
@click.option('--repo-slug', '-r', 'repo_slugs', multiple=True,
              help='Repository slug to update. Can be repeated. Webhook is only created for those repositories.')
@click.option('--project-key', '-p',
              help='Projet key. Webhook is updated for all repositories part of this project.')
@click.option('--delete', '-d', is_flag=True,
              help='Delete the webhook.')
@click.option('--update', '-u', is_flag=True,
              help='Update the webhook.')
@click.pass_obj
def bitbucket(filepath, name, repo_slugs, project_key, delete, update):
    """Modify a webhook on the specified Bitbucket repositories"""
    client = Bitbucket(filepath)
    if not repo_slugs:
        repo_slugs = client.get_repo_slugs(project_key)
    if delete:
        client.delete_webhooks(name, repo_slugs)
    else:
        client.create_webhooks(name, repo_slugs, update)


@cli.command()
@click.argument('name')
@click.option('--project-path', '-p', 'project_paths', multiple=True,
              help='Project path to update. Can be repeated. Webhook is only created for those repositories.')
@click.option('--namespace', '-n',
              help='Namespace. Webhook is updated for all repositories part of this namespace.')
@click.option('--delete', '-d', is_flag=True,
              help='Delete the webhook.')
@click.option('--update', '-u', is_flag=True,
              help='Update the webhook.')
@click.pass_obj
def gitlab(filepath, name, project_paths, namespace, delete, update):
    """Modify a webhook on the specified Gitlab repositories"""
    client = Gitlab(filepath)
    if not project_paths:
        project_paths = client.get_project_paths(namespace)
    project_paths = [quote_plus(project_path) for project_path in project_paths]
    if delete:
        client.delete_webhooks(name, project_paths)
    else:
        client.create_webhooks(name, project_paths, update)
