import logging
import requests
import sys
from requests.exceptions import HTTPError
from .common import GitServer

try:
    # Python 3
    from urllib.parse import urlencode
except ImportError:
    # Python 2
    from urllib import urlencode


logger = logging.getLogger('py-webhook.gitlab')


class Gitlab(GitServer):

    def __init__(self, conf):
        super(Gitlab, self).__init__(conf)
        gitlab = self.config['gitlab']
        self.api_url = gitlab['url'] + '/api/v4'
        self.session = requests.Session()
        self.session.headers.update({'Private-Token': gitlab['token']})

    def request(self, method, endpoint, **kwargs):
        r = self.session.request(method, self.api_url + endpoint, **kwargs)
        r.raise_for_status()
        return r

    def get(self, endpoint, **kwargs):
        return self.request('GET', endpoint, **kwargs)

    def post(self, endpoint, **kwargs):
        return self.request('POST', endpoint, **kwargs)

    def put(self, endpoint, **kwargs):
        return self.request('PUT', endpoint, **kwargs)

    def delete(self, endpoint, **kwargs):
        return self.request('DELETE', endpoint, **kwargs)

    def get_all(self, endpoint, **kwargs):
        """Generator that returns all items from the endpoint"""
        r = self.get(endpoint, **kwargs)
        for item in r.json():
            yield item
        while 'next' in r.links:
            url = r.links['next']['url']
            r = self.session.get(url)
            r.raise_for_status()
            for item in r.json():
                yield item

    def get_project_paths(self, group):
        """Return all projects part of this group"""
        for project in self.get_all('/groups/{}/projects'.format(group)):
            yield project['path_with_namespace']

    def create_webhook(self, project_path, data, name):
        try:
            r = self.post('/projects/{}/hooks'.format(project_path), json=data)
        except HTTPError:
            logger.warning('Webhook "{}" NOT created for {}: {}'.format(name, project_path, r.text))
        else:
            logger.info('Webhook "{}" successfully created for {}'.format(
                name, project_path))

    def update_webhook(self, project_path, hook_id, data, name):
        try:
            r = self.put('/projects/{}/hooks/{}'.format(project_path, hook_id), json=data)
        except HTTPError:
            logger.warning('Webhook "{}" NOT updated for {}: {}'.format(name, project_path, r.text))
        else:
            logger.info('Webhook "{}" successfully created for {}'.format(
                name, project_path))

    def webhook_id(self, project_path, url):
        """Return the webhook id from the project or None"""
        for hook in self.get_all('/projects/{}/hooks'.format(project_path)):
            if hook['url'].startswith(url):
                    return hook['id']
        return None

    def delete_webhook(self, project_path, hook_id):
        r = self.delete('/projects/{}/hooks/{}'.format(project_path, hook_id))
        if r.status_code == 204:
            logger.info('Webhook "{}" successfully deleted for {}'.format(
                hook_id, project_path))
        else:
            logger.warning('Webhook "{}" NOT deleted for {}: {}'.format(hook_id, project_path, r.text))

    def delete_webhooks(self, name, project_paths):
        """Delete the webhook on all repositories passed in parameters"""
        webhook_info = self.webhook_from_conf(name)
        for project_path in project_paths:
            hook_id = self.webhook_id(project_path, webhook_info['url'])
            if hook_id is None:
                logger.info("Skipping {}. Webhook '{}' doesn't exist.".format(project_path, name))
            else:
                self.delete_webhook(project_path, hook_id)

    def create_webhooks(self, name, project_paths, update=False):
        """Create a webhook on all repositories passed in parameters"""
        webhook_info = self.webhook_from_conf(name)
        if webhook_info is None:
            logger.error('Unknown webhook "{}". Check your configuration. Abort.'.format(name))
            sys.exit(1)
        query_string = urlencode(webhook_info['args'])
        events = webhook_info.get('events', {})
        data = {
            'url': '{url}?{query_string}'.format(url=webhook_info['url'], query_string=query_string),
            'push_events': True,
            'tag_push_events': True,
        }
        data.update(events)
        for project_path in project_paths:
            if project_path in webhook_info.get('exclude', []):
                logger.info('Skipping {}. Repository is excluded from {}.'.format(project_path, name))
                continue
            hook_id = self.webhook_id(project_path, webhook_info['url'])
            if hook_id is not None:
                if update:
                    self.update_webhook(project_path, hook_id, data, name)
                else:
                    logger.info('Skipping {}. Webhook "{}" already exists.'.format(project_path, name))
            else:
                self.create_webhook(project_path, data, name)
