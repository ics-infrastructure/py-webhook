import abc
import yaml


class GitServer(object):
    __metaclass__ = abc.ABCMeta

    def __init__(self, conf):
        with open(conf, 'r') as f:
            self.config = yaml.load(f)
        self.webhooks = self.config.get('webhooks', [])

    def webhook_from_conf(self, name):
        """Return the webhook information from the config"""
        for webhook in self.webhooks:
            if name == webhook['name']:
                return webhook
        return None

    @abc.abstractmethod
    def delete_webhooks(self, name, repo_slugs):
        """Delete the webhook on all repositories passed in parameters"""

    @abc.abstractmethod
    def create_webhooks(self, name, repo_slugs, update=False):
        """Create a webhook on all repositories passed in parameters"""
