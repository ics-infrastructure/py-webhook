import logging
import sys
from oauthlib.oauth2 import BackendApplicationClient
from requests_oauthlib import OAuth2Session
from .common import GitServer

try:
    # Python 3
    from urllib.parse import urlencode
except ImportError:
    # Python 2
    from urllib import urlencode


BITBUCKET_API_URL = 'https://api.bitbucket.org/2.0'
BITBUCKET_TOKEN_URL = 'https://bitbucket.org/site/oauth2/access_token'
logger = logging.getLogger('py-webhook.bitbucket')


class Bitbucket(GitServer):

    def __init__(self, conf):
        super(Bitbucket, self).__init__(conf)
        self.bitbucket = self.config['bitbucket']
        client = BackendApplicationClient(client_id=self.bitbucket['client_id'])
        self.oauth = OAuth2Session(client=client)
        self.oauth.fetch_token(token_url=BITBUCKET_TOKEN_URL,
                               client_id=self.bitbucket['client_id'],
                               client_secret=self.bitbucket['client_secret'])

    def pages(self, url, params=None):
        """Generator that returns all pages from a paginated API"""
        r = self.oauth.get(url, params=params)
        r.raise_for_status()
        if r.status_code == 200:
            data = r.json()
            yield data
            while 'next' in data:
                r = self.oauth.get(data['next'])
                r.raise_for_status()
                data = r.json()
                yield data

    def get_repo_slugs(self, project_key):
        """Return all repositories slug part of the project"""
        url = '{api_url}/repositories/{owner}'.format(
            api_url=BITBUCKET_API_URL, owner=self.bitbucket['owner'])
        for page in self.pages(url, params={'q': 'project.key="{}"'.format(project_key)}):
            for repo in page['values']:
                # Temporary check
                if repo['project']['key'] != project_key:
                    logger.error('Invalid project key: {}'.format(repo['project']['key']))
                else:
                    yield repo['slug']

    def webhook(self, name, slug):
        """Return the webhook from the repository or None"""
        url = '{api_url}/repositories/{owner}/{slug}/hooks'.format(
            api_url=BITBUCKET_API_URL, owner=self.bitbucket['owner'], slug=slug)
        for page in self.pages(url):
            for webhook in page['values']:
                if webhook['description'] == name:
                    return webhook
        return None

    def create_webhook(self, data, slug):
        url = '{api_url}/repositories/{owner}/{slug}/hooks'.format(
            api_url=BITBUCKET_API_URL, owner=self.bitbucket['owner'], slug=slug)
        r = self.oauth.post(url, json=data)
        if r.status_code == 201:
            logger.info('Webhook "{}" successfully created for {}'.format(
                data['description'], slug))
        else:
            logger.warning('Webhook "{}" NOT created for {}: {}'.format(data['description'], slug, r.text))

    def update_webhook(self, data, slug, uuid):
        url = '{api_url}/repositories/{owner}/{slug}/hooks/{uuid}'.format(
            api_url=BITBUCKET_API_URL, owner=self.bitbucket['owner'], slug=slug, uuid=uuid)
        r = self.oauth.put(url, json=data)
        if r.status_code == 200:
            logger.info('Webhook "{}" successfully updated for {}'.format(
                data['description'], slug))
        else:
            logger.warning('Webhook "{}" NOT updated for {}: {}'.format(data['description'], slug, r.text))

    def delete_webhook(self, slug, uuid):
        url = '{api_url}/repositories/{owner}/{slug}/hooks/{uuid}'.format(
            api_url=BITBUCKET_API_URL, owner=self.bitbucket['owner'], slug=slug, uuid=uuid)
        r = self.oauth.delete(url)
        if r.status_code == 204:
            logger.info('Webhook "{}" successfully deleted for {}'.format(
                uuid, slug))
        else:
            logger.warning('Webhook "{}" NOT deleted for {}: {}'.format(uuid, slug, r.text))

    def create_webhooks(self, name, repo_slugs, update=False):
        """Create a webhook on all repositories passed in parameters"""
        webhook_info = self.webhook_from_conf(name)
        if webhook_info is None:
            logger.error('Unknown webhook "{}". Check your configuration. Abort.'.format(name))
            sys.exit(1)
        query_string = urlencode(webhook_info['args'])
        data = {'description': name,
                'url': '{url}?{query_string}'.format(url=webhook_info['url'], query_string=query_string),
                'active': True,
                'events': ['repo:push'],
                }
        for slug in repo_slugs:
            if slug in webhook_info.get('exclude', []):
                logger.info('Skipping {}. Repository is excluded from {}.'.format(slug, name))
                continue
            webhook = self.webhook(name, slug)
            if webhook is not None:
                if update:
                    self.update_webhook(data, slug, webhook['uuid'])
                else:
                    logger.info('Skipping {}. Webhook "{}" already exists.'.format(slug, name))
            else:
                self.create_webhook(data, slug)

    def delete_webhooks(self, name, repo_slugs):
        """Delete the webhook on all repositories passed in parameters"""
        for slug in repo_slugs:
            webhook = self.webhook(name, slug)
            if webhook is None:
                logger.info("Skipping {}. Webhook '{}' doesn't exist.".format(slug, name))
            else:
                self.delete_webhook(slug, webhook['uuid'])
