from setuptools import setup, find_packages


setup(name='py-webhook',
      version='0.4.0',
      description='Python script to interact with Bitbucket and Gitlab webhooks',
      url='https://gitlab.esss.lu.se/ics-infrastructure/py-webhook.git',
      author='Benjamin Bertrand',
      author_email='benjamin.bertrand@esss.se',
      license='BSD',
      packages=find_packages(exclude=('tests',)),
      install_requires=[
          'Click',
          'PyYAML',
          'requests',
          'requests-oauthlib',
      ],
      entry_points='''
          [console_scripts]
          py-webhook=pywebhook.cmd:cli
      ''',
      setup_requires=['pytest-runner'],
      tests_require=[
          'pytest',
          'requests-mock',
      ]
      )
